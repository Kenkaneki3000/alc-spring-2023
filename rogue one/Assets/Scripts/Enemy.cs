using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Enemy : MonoBehaviour
{
    public int health;
    public int damage;
   
    public GameObject deathDropPrefab;
    public SpriteRenderer sr;
    public PlayerController2D player;
   
    public LayerMask moveLayerMask;
    public float delay = 0.05f;
    public float attackChance = 0.05f;
   
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController2D>();
    }

    public void Move()
    {
        if(Random.value < 0.5f)
            return;
           
        Vector3 dir = Vector3.zero;
        bool canMove = false;
       
        //before moving the enemy, get a random direction to move to
        while(!canMove)
        {
            dir = GetRandomDirection();
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);
           
            if(hit.collider == null)
                canMove = true;
               
        }
    transform.position += dir;
   
    }
   
    Vector3 GetRandomDirection()
    {
        //get a random number between 0 and 4
        int ran = Random.Range(0, 4);
       
        if(ran == 0)
            return Vector3.up;
        else if(ran == 1)
            return Vector3.down;
        else if(ran == 2)
            return Vector3.left;
        else if(ran == 3)
            return Vector3.right;
           
        return Vector3.zero;
    }

    public void TakeDamage(int damageToTake)
    {
        health -= damageToTake;
        if (health <= 0)
        {
            if(deathDropPrefab != null)
                Instantiate(deathDropPrefab, transform.position, transform.rotation);
               
            Destroy(gameObject);
           
        }
   
        StartCoroutine(DamageFlash());
       
        if(Random.value > attackChance)
            player.TakeDamage(damage);
    }
   
   
    IEnumerator DamageFlash()
    {
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.red;
        yield return new WaitForSeconds(delay);
        //set color back to default
        sr.color = defaultColor;
    }
}
