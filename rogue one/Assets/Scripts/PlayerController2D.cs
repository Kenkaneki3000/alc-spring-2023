using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController2D : MonoBehaviour
{
    public int curHP;
    public int maxHP;
    public int coins;

    public bool hasKey;

    public SpriteRenderer sr;

    // layer to avoid (mask)
    public LayerMask moveLayerMask;

    public float delay = 0.05f;

    public int damageAmount = 1;
    public float moveTileSize = 0.16f;

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);
        // If there is no moveLayerMask detected in front of the player
        if(hit.collider == null)
        {
            //Move forward
            transform.position += new Vector3(dir.x * moveTileSize, dir.y * moveTileSize, 0);
            // Move all the enemies
            EnemyManager.instance.OnPlayerMove();
        }
    }


    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }
    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }
    public void OnMoveLeft(InputAction.CallbackContext context)
    {
       if(context.phase == InputActionPhase.Performed)
            Move(Vector2.left); 
    }
    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }
    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.up);
    }
    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.down);
    }
    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.left);
    }
    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.right);
    }

    public void TakeDamage(int damageToTake)
    {
        curHP -= damageToTake;
        StartCoroutine(DamageFlash());

        if(curHP <= 0)
            SceneManager.LoadScene(0);
    }

    IEnumerator DamageFlash()
    {
        //get a reference to the default sprite color
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.white;
        // wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //Set the color back to it original color
        sr.color = defaultColor;
    }

    void TryAttack(Vector2 dir)
    {
        // Ignore the layer 1 to layer 6
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, 1 << 7);

        if(hit.collider != null)
        {
            hit.transform.GetComponent<Enemy>().TakeDamage(damageAmount);  
        }
    }

}  
