using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    [Header("Map Properties")]
    public int mapWidth = 5;
    public int mapHeight = 5;
    public int roomsToGenerate = 9;
   
    private int roomCount;
    private bool roomsInstantiated;
   
    //Store the origin of the first room to generate procedual dungeon
    private Vector2 firstRoomPos;
   
    // A 2D boolean array to map out the level
    private bool[,] map;
    // The room prefab to instantiate
    public GameObject roomPrefab;
   
    private List<Room> roomObjects = new List<Room>();
   
    // Creating Singleton
    public static Generation instance;
   
    void Awake()
    {
        instance = this;
    }
   
    // Start is called before the first frame update
    void Start()
    {
        //Random seed assigned to random number generator
        Random.InitState(74742584);
        Generate();
    }

    public void Generate()
    {
        //Create a new map of the specified size
        map = new bool[mapWidth, mapHeight];
        //Check to see if we can place a room in the center of the map
        CheckRoom(3, 3, 0, Vector2.zero, true);
        InstantiateRooms();
        //Find the playa in the scene, and position them inside the first room
        FindObjectOfType<PlayerController2D>().transform.position = firstRoomPos * 12;
    }
   
    void CheckRoom(int x, int y, int remaining, Vector2 generalDirection, bool firstRoom = false)
    {
        // North will be true if the random value is greater than 0.2f (if generalDirection is == up) or greater than 0.8f (When generalDirection is != up)
        bool north = Random.value > (generalDirection == Vector2.up ? 0.2f : 0.8f);
        bool south = Random.value > (generalDirection == Vector2.down ? 0.2f : 0.8f);
        bool east = Random.value > (generalDirection == Vector2.right ? 0.2f : 0.8f);
        bool west = Random.value > (generalDirection == Vector2.left ? 0.2f : 0.8f);
   
        int maxRemaining = roomsToGenerate / 4;
   
        //If genralDirection is true, make a room one tile north, south, east, or west the current
        if(north || firstRoom)
            CheckRoom(x, y + 1, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.up : generalDirection);

        if(south || firstRoom)
            CheckRoom(x, y - 1, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.down : generalDirection);

        if(east || firstRoom)
            CheckRoom(x + 1, y, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.right : generalDirection);

        if(west || firstRoom)
            CheckRoom(x - 1, y, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.left : generalDirection);

        //If have generated all of the rooms that we need, stop checking the rooms
        if(roomCount >= roomsToGenerate)
            return;
           
        //If this is outside the bounds of the actual map, stop the function
        if(x < 0 || x > mapWidth - 1 || y < 0 ||  y > mapHeight - 1)
            return;
             
        //If this is not the first room, and there is no more room to check, stop the function
        if(firstRoom == false && remaining <= 0)
            return;
         
        //If the given map tile is already occupied, stop the function
        if(map[x, y] == true)
            return;
           
        //If this is the first room, store the room position
        if(firstRoom == true)
            firstRoomPos = new Vector2(x, y);
           
        //Add one to roomCount and set the map tile to be true
        roomCount++;
        map[x, y] = true;
       
       
    }
   
    void InstantiateRooms()
    {
        if(roomsInstantiated)
            return;
       
        roomsInstantiated = true;
       
        for(int x = 0; x < mapWidth; ++x)
        {
            for(int y = 0; y < mapHeight; ++y)
            {
                if(map[x,y] == false)
                    continue;
                   
                //instantiate a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x,y,0)* 12, Quaternion.identity);
               
                //Get reference to the room script of the new room object
                Room room = roomObj.GetComponent<Room>();
                   
                //If we're within (the boundary) of the map, and if there is room above of us
                if(y < mapHeight - 1 && map[x,y + 1] == true)
                {
                    //Enable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                }
                //If we're within (the boundary) of the map, and if there is room below of us
                if(y > 0 && map[x,y -1] == true)
                {
                    //Enable the south door and disable the south wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                }
                //If we're within (the boundary) of the map, and if there is room right of us
                if(x < mapWidth - 1 && map[x + 1,y] == true)
                {
                    //Enable the north door and disable the east wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                }
                //If we're within (the boundary) of the map, and if there is room left of us
                if(x > 0 && map[x -1,y] == true)
                {
                    //Enable the north door and disable the north wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                }
                
                //if this is not the first room, call generation
                if(firstRoomPos != new Vector2(x,y))
                    room.GenerateInterior();
                
                //is this not the fisrst rom to the roomObject list
                roomObjects.Add(room);
            }
        }
        //After looping through ever pleasent inside the map array, call calculayekeyandexit
        CalculateKeyandExit();
    }

   //place the key and exit in the level
    void CalculateKeyandExit()
    {
       
    }
}
