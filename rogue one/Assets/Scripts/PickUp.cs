using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickupType
{
    Coin,
    Health
}

public class PickUp : MonoBehaviour
{
    public PickupType type;
    public int value = 1;

   private void OnTriggerEnter2D(Collider2D other) 
   {
     //if the pickup object has collided with the player
     if(other.CompareTag("Player"))
     {
         //if the pickup type is a coin
         if(type== PickupType.Coin)
         {

         }
         //if the pickup type is Health
         if(type == PickupType.Health);
     }   

   }

}
