using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
     //Reference for the door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;
    //Reference for the wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;
    //how many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;
    //Object to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject keyPrefab;
    public GameObject exitDoorPrefab;
    // list of postions to avoid instantiating new objects on top of old
    private List<Vector3> usedPositions = new List<Vector3>();
    

    public void GenerateInterior()
    {
        // Generate coins,enemies, health packs, and tacos, etc.
    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
